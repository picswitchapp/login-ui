package com.parse.ui;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Fragment for the user signup screen.
 */
public class PhoneNumberVerificationFragment extends ParseLoginFragmentBase {

	public interface PhoneNumberVerificationFragmentListener {
		public void onEnterCodeClicked(String firstName, String lastName,String password, String email, String birthyear, String gender, String id, String phoneNumber);

		public void onLoginHelpClicked();

		public void onLoginSuccess();
	}

	public static final String FIRSTNAME = "com.parse.ui.PhoneNumberVerificationFragment.FIRSTNAME";
	public static final String LASTNAME = "com.parse.ui.PhoneNumberVerificationFragment.LASTNAME";
	public static final String PASSWORD = "com.parse.ui.PhoneNumberVerificationFragment.PASSWORD";
	public static final String EMAIL = "com.parse.ui.PhoneNumberVerificationFragment.EMAIL";
	public static final String BIRTHYEAR = "com.parse.ui.PhoneNumberVerificationFragment.BIRTHYEAR";
	public static final String GENDER = "com.parse.ui.PhoneNumberVerificationFragment.GENDER";
	private static final String[] countries = {
		"USA", 
		"Australia", 
		"Canada",
		"China", 
		"Hong Kong",
		"India", 
		"Japan", 
		"Kenya",
		"New Zealand", 
		"Nigeria",
		"South Africa", 
		"South Korea",
		"United Kingdom"};
	private PhoneNumberVerificationFragmentListener numVerificationListener;
	//incoming call detector
	IncomingCallDetector phoneListener;
	public boolean takeLock = false;
	private String incomingCallNumber = "";
	boolean validationStatus = false;
	private Object lock = new Object();
	private TextView phoneNumberField;
	private String countryCodeSelected = "";
	private Button verifyPhoneNumberButton;
	private Spinner selectCountry;
	private ParseOnLoginSuccessListener onLoginSuccessListener;

	private ParseLoginConfig config;
	private int minPhoneNumberLength;

	private static final String LOG_TAG = "PhoneNumberVerification";
	private static final String USER_OBJECT_NAME_FIELD = "name";
	private static final int DEFAULT_MIN_PHONENUMBER_LENGTH = 8;
	private String firstName = "";
	private String lastName = "";
	private String password = "";
	private String email = "";
	private String birthyear = "";
	private String gender = "";
	private String phoneNumber = "";
	private String country = "";

	Typeface varella;
	Typeface montserrat;
	ProgressDialog pDialog;

	public static PhoneNumberVerificationFragment newInstance(Bundle configOptions, 
			String firstName, String lastName,String password, String email, String birthyear, String gender ) {
		PhoneNumberVerificationFragment signupFragment = new PhoneNumberVerificationFragment();
		Bundle args = new Bundle(configOptions);
		args.putString(PhoneNumberVerificationFragment.FIRSTNAME, firstName);
		args.putString(PhoneNumberVerificationFragment.LASTNAME, lastName);
		args.putString(PhoneNumberVerificationFragment.PASSWORD, password);
		args.putString(PhoneNumberVerificationFragment.EMAIL, email);
		args.putString(PhoneNumberVerificationFragment.BIRTHYEAR, birthyear);
		args.putString(PhoneNumberVerificationFragment.GENDER, gender);
		signupFragment.setArguments(args);
		return signupFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup parent,
			Bundle savedInstanceState) {

		Bundle args = getArguments();
		config = ParseLoginConfig.fromBundle(args, getActivity());
		minPhoneNumberLength = DEFAULT_MIN_PHONENUMBER_LENGTH;

		View v = inflater.inflate(R.layout.phone_number_fragment,
				parent, false);
		varella = Typeface.createFromAsset(parent.getContext().getAssets(), "VarelaRound.ttf");
		montserrat = Typeface.createFromAsset(parent.getContext().getAssets(), "Montserrat-Regular.ttf");
		ImageView appLogo = (ImageView) v.findViewById(R.id.app_logo);

		TextView instructions = (TextView) v.findViewById(R.id.phone_number_instructions);
		instructions.setTypeface(varella);
		final TextView countryCode = (TextView) v.findViewById(R.id.country_code);
		countryCode.setTypeface(varella);

		/*Select country spinner*/
		selectCountry = (Spinner)v.findViewById(R.id.select_country);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, countries);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectCountry.setAdapter(new MySpinnerAdapter(
				adapter,
				R.layout.select_country_spinner_header,
				getActivity()));
		selectCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				country = (String) parent.getItemAtPosition(position);
				if(country == "USA" || country == "Canada"){
					countryCodeSelected = "+1";
				}else if(country == "United Kingdom"){
					countryCodeSelected = "+44";
				}else if(country == "Kenya"){
					countryCodeSelected = "+254";
				}else if(country == "Japan"){
					countryCodeSelected = "+81";
				}else if(country == "South Africa"){
					countryCodeSelected = "+27";
				}else if(country == "Hong Kong"){
					countryCodeSelected = "+852";
				}else if(country == "India"){
					countryCodeSelected = "+91";
				}
				else if(country == "South Korea"){
					countryCodeSelected = "+82";
				}else if(country == "New Zealand"){
					countryCodeSelected = "+64";
				}else if(country == "China"){
					countryCodeSelected = "+86";
				}else if(country == "Australia"){
					countryCodeSelected = "+61";
				}
				else if(country == "Nigeria"){
					countryCodeSelected = "+234";
				}
				countryCode.setText(countryCodeSelected);

				TextView newText = (TextView) parent.getChildAt(0);
				if(newText != null){
					newText.setTextColor(Color.WHITE);
					newText.setTypeface(varella);
				}
				System.out.println("country selected = " + country);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				country = "";
				System.out.println("country not selected = " + country);
			}
		});
		/*End of select country spinner*/


		TextView phoneDisclaimer = (TextView) v.findViewById(R.id.phone_number_disclaimer);
		phoneDisclaimer.setText(Html.fromHtml("We require phone validation so that your friends can find you.  It also prevents spam.  We do not share your personal information with third parties, and SMS charges may apply."));
		phoneDisclaimer.setTypeface(varella);

		phoneNumberField = (TextView) v.findViewById(R.id.phone_number_input);
		phoneNumberField.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		phoneNumberField.setTypeface(varella);

		verifyPhoneNumberButton = (Button) v.findViewById(R.id.confirm_phone_number_button);
		verifyPhoneNumberButton.setTypeface(montserrat);


		if (appLogo != null && config.getAppLogo() != null) {
			appLogo.setImageResource(config.getAppLogo());
		}

		verifyPhoneNumberButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				//				String phoneEntered = phoneNumberField.getText().toString();
				//				phoneEntered = phoneEntered.replace(")", "").replace("(", "").replaceAll(" ", "").replace("-", "");

				if(countryCodeSelected.length() > 0){
					phoneNumber = countryCodeSelected + phoneNumberField.getText().toString();


					/**International formatting*/
					PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
					PhoneNumber formattedPhoneNumber = null;
					try {
						formattedPhoneNumber = phoneUtil.parse(phoneNumber, "");
					} catch (NumberParseException e) {
						
						System.err.println("NumberParseException was thrown: " + e.toString());
					}

					System.out.println("PHONENUMBER:" + phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL));
					phoneNumber = phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL);

					System.out.println("phoneEntered.length" + phoneNumber.length());

					AlertDialog.Builder mBuilder = new AlertDialog.Builder(parent.getContext());
					mBuilder.setTitle("Confirm");
					if(phoneUtil.isValidNumber(formattedPhoneNumber)){
						mBuilder.setMessage("You entered " + phoneNumber + ".  Is this correct?");
						mBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								pDialog = new ProgressDialog(parent.getContext());
								pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
								pDialog.setMessage("Sending verification code");
								pDialog.show();
								verifyPhoneNumber();
							}
						});
						mBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
					}else{
						mBuilder.setMessage("Please enter a valid phone number");
						mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
					}

					AlertDialog mDialog = mBuilder.create();
					mDialog.show();
				}else{
					showToast("Please select your country.");
				}

			}
		});

		firstName = (String) args.getString(FIRSTNAME);
		lastName = (String) args.getString(LASTNAME);
		password = (String) args.getString(PASSWORD);
		email = (String) args.getString(EMAIL);
		birthyear = (String) args.getString(BIRTHYEAR);
		gender = (String) args.getString(GENDER);

		//incoming call detector
		phoneListener = new IncomingCallDetector(getActivity());
		TelephonyManager telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneListener,
				IncomingCallDetector.LISTEN_CALL_STATE);

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof ParseOnLoginSuccessListener) {
			onLoginSuccessListener = (ParseOnLoginSuccessListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}

		if (activity instanceof ParseOnLoginSuccessListener) {
			numVerificationListener = (PhoneNumberVerificationFragmentListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}

		if (activity instanceof ParseOnLoadingListener) {
			onLoadingListener = (ParseOnLoadingListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoadingListener");
		}
	}



	public void verifyPhoneNumber() {
		//		phoneNumber = countryCodeSelected + phoneNumberField.getText().toString();
		//
		//		/**International formatting*/
		//		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		//		PhoneNumber formattedPhoneNumber = null;
		//		try {
		//			formattedPhoneNumber = phoneUtil.parse(phoneNumber, "");
		//		} catch (NumberParseException e) {
		//			System.err.println("NumberParseException was thrown: " + e.toString());
		//		}
		//
		//		System.out.println("PHONENUMBER:" + phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL));
		//		phoneNumber = phoneUtil.format(formattedPhoneNumber, PhoneNumberFormat.INTERNATIONAL);
		phoneNumber = phoneNumber.replaceAll("\\D+","");
		/*End of international formatting*/

		System.out.println("PHONENUMBER: " + phoneNumber);
		if (phoneNumber!= null && phoneNumber.length() == 0) {
			showToast("Please enter a valid phone number to continue");
		} else if (phoneNumber.length() < minPhoneNumberLength) {
			showToast("Phone Number is too short");
		}
		else {
			HashMap<String, String> verification = new HashMap<String, String>();
			verification.put("phoneNumber", phoneNumber); 
			verification.put("language", "en-US");
			verification.put("type", "sms");
			ParseCloud.callFunctionInBackground("PVRequestAndroid", verification, new FunctionCallback<Map<String, String>>() {

				@Override
				public void done(Map<String, String> data, ParseException e) {
					//phoneListener.loadingScreen(data, email, password);
					//verifyPhoneNumberButton.setText("CREATE ACCOUNT");
					if(pDialog.isShowing()){
						pDialog.cancel();
					}

					if(e == null){
						//go on to next fragment
						numVerificationListener.onEnterCodeClicked(firstName, lastName, password, email, birthyear, gender, data.get("id"), phoneNumber);
					}else{
						e.printStackTrace();
						showToast("Error: please try again");
					}
				}
			});

		}
	}



	@Override
	protected String getLogTag() {
		return LOG_TAG;
	}

	private void signupSuccess() {
		onLoginSuccessListener.onLoginSuccess();
	}

	public void createAccount(){
		ParseUser user = new ParseUser();

		// Set standard fields
		user.setUsername(email);
		user.setPassword(password);
		user.setEmail(email);
		user.put("phoneNumber", "1" + phoneNumber); //add country code later

		// Set additional custom fields only if the user filled it out
		if (firstName.length() != 0) {
			user.put("firstName", firstName);
		}
		if (lastName.length() != 0) {
			user.put("lastName", lastName);
		}
		if (gender.length() != 0) {
			user.put("gender", gender);
		}
		if (birthyear.length() != 0) {
			user.put("age", Integer.parseInt(birthyear.trim()));
		}
		loadingStart();
		user.signUpInBackground(new SignUpCallback() {

			@Override
			public void done(ParseException e) {
				if (isActivityDestroyed()) {
					return;
				}

				if (e == null) {
					loadingFinish();

					signupSuccess();
					HashMap<String, String> params = new HashMap<String, String>();
					ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
						@Override
						public void done(String object, ParseException e) {

						}
					});
				} else {
					loadingFinish();
					if (e != null) {
						debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
								e.toString());
						switch (e.getCode()) {
						//						case ParseException.INVALID_EMAIL_ADDRESS:
						//							showToast(R.string.com_parse_ui_invalid_email_toast);
						//							break;
						case ParseException.USERNAME_TAKEN:
							showToast(R.string.com_parse_ui_username_taken_toast);
							break;
						case ParseException.EMAIL_TAKEN:
							showToast(R.string.com_parse_ui_email_taken_toast);
							break;
						default:
							showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
						}
					}
				}
			}
		});
	}


	public void waitForCall(){
		if(!phoneListener.dialog.isShowing()){
			createAccount();
		}
		else{
			//use button with changing text
		}
	}
}
