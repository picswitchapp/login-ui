/*
 *  Copyright (c) 2014, Facebook, Inc. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Facebook.
 *
 *  As with any software that integrates with the Facebook platform, your use of
 *  this software is subject to the Facebook Developer Principles and Policies
 *  [http://developers.facebook.com/policy/]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.parse.ui;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Fragment for the user signup screen.
 */
public class ParseSignupFragment extends ParseLoginFragmentBase {
	public interface ParseSignUpFragmentListener {
		public void onVerifyPhoneNumberClicked(String firstName, String lastName, String password, String email, String age, String gender);

		public void onLoginHelpClicked();

		public void onLoginSuccess();
	}

	public static final String USERNAME = "com.parse.ui.ParseSignupFragment.USERNAME";
	public static final String PASSWORD = "com.parse.ui.ParseSignupFragment.PASSWORD";
	public static final String PICSWITCHUSERNAME = "com.parse.ui.ParseSignupFragment.PICSWITCHUSERNAME";

	private EditText birthyearField;
	private Spinner genderField;
	private String gender = "";
	private static final String[] sexes = {"Male", "Female", "I wish to not disclose"};
	private EditText firstNameField;
	private EditText lastNameField;
	private Button nextButtonToVerifyPhone;
	private ParseOnLoginSuccessListener onLoginSuccessListener;
	private ParseSignUpFragmentListener signUpFragmentListener; 
	private String username = "";
	private String password = "";
	private String picswitchUsername = "";
	private ParseLoginConfig config;
	private int minPasswordLength;
	private Context mContext;

	private static final String LOG_TAG = "ParseSignupFragment";
	private static final int DEFAULT_MIN_PASSWORD_LENGTH = 6;
	private static final String USER_OBJECT_NAME_FIELD = "name";


	public static ParseSignupFragment newInstance(Bundle configOptions, String username, String password, String picswitchUsername) {
		ParseSignupFragment signupFragment = new ParseSignupFragment();
		Bundle args = new Bundle(configOptions);
		args.putString(ParseSignupFragment.USERNAME, username);
		args.putString(ParseSignupFragment.PASSWORD, password);
		args.putString(ParseSignupFragment.PICSWITCHUSERNAME, picswitchUsername);
		signupFragment.setArguments(args);
		return signupFragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {

		Bundle args = getArguments();
		config = ParseLoginConfig.fromBundle(args, getActivity());
		mContext = parent.getContext();
		minPasswordLength = DEFAULT_MIN_PASSWORD_LENGTH;
		if (config.getParseSignupMinPasswordLength() != null) {
			minPasswordLength = config.getParseSignupMinPasswordLength();
		}

		username = (String) args.getString(USERNAME);
		password = (String) args.getString(PASSWORD);
		picswitchUsername = (String)args.getString(PICSWITCHUSERNAME);
		View v = inflater.inflate(R.layout.com_parse_ui_parse_signup_fragment,
				parent, false);

		final Typeface varella = Typeface.createFromAsset(parent.getContext().getAssets(), "VarelaRound.ttf");
		final Typeface montserrat = Typeface.createFromAsset(parent.getContext().getAssets(), "Montserrat-Regular.ttf");

		TextView createInstructions = (TextView) v.findViewById(R.id.create_an_account);
		createInstructions.setTypeface(montserrat);

		ImageView appLogo = (ImageView) v.findViewById(R.id.app_logo);
		genderField = (Spinner) v.findViewById(R.id.signup_gender_input);


		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, sexes);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//genderField.setPrompt("Select a gender");
		genderField.setAdapter(new MySpinnerAdapter(
				adapter,
				R.layout.contact_spinner_row_nothing_selected,
				getActivity()));
		genderField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				gender = (String) parent.getItemAtPosition(position);
				TextView newText = (TextView) parent.getChildAt(0);
				if(newText != null){
					newText.setTextColor(Color.WHITE);
					newText.setTypeface(varella);
				}
				System.out.println("something selected gender = " + gender);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				gender = "";
				System.out.println("nothing selected gender = " + gender);
			}
		});



		birthyearField = (EditText) v.findViewById(R.id.signup_year_input);
		birthyearField.setTypeface(varella);

		firstNameField = (EditText) v.findViewById(R.id.signup_first_name_input);
		lastNameField = (EditText) v.findViewById(R.id.signup_last_name_input);
		nextButtonToVerifyPhone = (Button) v.findViewById(R.id.create_account);

		firstNameField.setTypeface(varella);
		lastNameField.setTypeface(varella);
		nextButtonToVerifyPhone.setTypeface(montserrat);

		if (appLogo != null && config.getAppLogo() != null) {
			appLogo.setImageResource(config.getAppLogo());
		}

		if (config.getParseSignupSubmitButtonText() != null) {
			nextButtonToVerifyPhone.setText(config.getParseSignupSubmitButtonText());
		}
		nextButtonToVerifyPhone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createAccountCloudFunction();
			}
		});

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof ParseOnLoginSuccessListener) {
			onLoginSuccessListener = (ParseOnLoginSuccessListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}
		if (activity instanceof ParseOnLoginSuccessListener) {
			signUpFragmentListener = (ParseSignUpFragmentListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}

		if (activity instanceof ParseOnLoadingListener) {
			onLoadingListener = (ParseOnLoadingListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoadingListener");
		}
	}

	//	signUpFragmentListener.onVerifyPhoneNumberClicked(firstName, lastName, password, username, birthyear, gender);

	public void createAccount(View v) {

		String firstName = firstNameField.getText().toString(), lastName = lastNameField.getText().toString();
		if (firstName != null) {
			firstName = firstNameField.getText().toString();
		}
		if (lastName != null) {
			lastName = lastNameField.getText().toString();
		}
		String birthyear = birthyearField.getText().toString();

		if (firstName != null && firstName.length() == 0) {
			showToast(R.string.com_parse_ui_no_name_toast);
		}
		else if (lastName != null && lastName.length() == 0) {
			showToast(R.string.com_parse_ui_no_name_toast);
		}
		else if ( (birthyear != null && birthyear.length() == 0) || (Integer.parseInt(birthyear) > 115) ) {
			showToast("Please enter a valid age to continue");
		}

		else if ( (gender != null && gender.length() == 0) || (gender == null)) {
			showToast("Please select your gender to continue");
		} 

		//		else{
		//
		//			signUpFragmentListener.onVerifyPhoneNumberClicked(firstName, lastName, password, username, birthyear, gender);
		//		}
		//signUpFragmentListener.onVerifyPhoneNumberClicked(firstName, lastName, password, email);
		else {
			ParseUser user = new ParseUser();

			// Set standard fields
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(username);
			user.put("picswitchUsername", picswitchUsername);

			// Set additional custom fields only if the user filled it out
			if (firstName.length() != 0) {
				user.put("firstName", firstName);
			}
			if (lastName.length() != 0) {
				user.put("lastName", lastName);
			}
			loadingStart();
			user.signUpInBackground(new SignUpCallback() {

				@Override
				public void done(ParseException e) {
					if (isActivityDestroyed()) {
						return;
					}

					if (e == null) {
						loadingFinish();
						signupSuccess();
					} else {
						loadingFinish();
						if (e != null) {
							debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
									e.toString());
							switch (e.getCode()) {
							case ParseException.INVALID_EMAIL_ADDRESS:
								showToast(R.string.com_parse_ui_invalid_email_toast);
								break;
							case ParseException.USERNAME_TAKEN:
								showToast(R.string.com_parse_ui_username_taken_toast);
								break;
							case ParseException.EMAIL_TAKEN:
								showToast(R.string.com_parse_ui_email_taken_toast);
								break;
							default:
								showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
							}
						}
					}
				}
			});
		}
	}


	public void createAccountCloudFunction(){
		String firstName = firstNameField.getText().toString(), lastName = lastNameField.getText().toString();
		if (firstName != null) {
			firstName = firstNameField.getText().toString();
		}
		if (lastName != null) {
			lastName = lastNameField.getText().toString();
		}
		String birthyear = birthyearField.getText().toString();

		if (firstName != null && firstName.length() == 0) {
			showToast(R.string.com_parse_ui_no_name_toast);
		}
		else if (lastName != null && lastName.length() == 0) {
			showToast(R.string.com_parse_ui_no_name_toast);
		}
		else if ( (birthyear != null && birthyear.length() == 0) || (Integer.parseInt(birthyear) > 115) ) {
			showToast("Please enter a valid age to continue");
		}

		else if ( (gender != null && gender.length() == 0) || (gender == null)) {
			showToast("Please select your gender to continue");
		} 
		else{
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("email", username);
			params.put("password", password);
			params.put("firstname", firstName);
			params.put("lastname", lastName);
			params.put("gender", gender);
			params.put("age", Integer.parseInt(birthyear.trim()));
			params.put("picswitchUsername", picswitchUsername);
			params.put("installationId", ParseInstallation.getCurrentInstallation().getInstallationId());
			ParseCloud.callFunctionInBackground("signupNew", params, new FunctionCallback<Boolean>() {

				@Override
				public void done(Boolean success, ParseException e) {
					if (isActivityDestroyed()) {
						return;
					}

					if(e == null){
						if(success){ 
							//successfully signed up user

							final ProgressDialog mDialog = new ProgressDialog(mContext);
							mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
							mDialog.setMessage("Creating account...");
							mDialog.show();

							ParseUser.logInInBackground(username, password, new LogInCallback() {
								@Override
								public void done(ParseUser user, ParseException e) {

									if (isActivityDestroyed()) {
										return;
									}

									if(e == null){
										HashMap<String, String> params = new HashMap<String, String>();
										ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
											@Override
											public void done(String object, ParseException e1) {
												if(mDialog.isShowing()){
													mDialog.cancel();
												}
												if(e1 == null){
													//logged in, so return

													loadingFinish();
													signupSuccess();

												}else{
													e1.printStackTrace();
													showToast("Error creating account, please try again.");
												}

											}
										});
									}else{
										e.printStackTrace();
										showToast("Error logging in.  Please try again.");
									}
								}
							});

							//new LogInAsyncTask(mContext).execute();
						}
					}else{
						e.printStackTrace();
						loadingFinish();
						debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
								e.toString());
						switch (e.getCode()) {
						//						case ParseException.INVALID_EMAIL_ADDRESS:
						//							showToast(R.string.com_parse_ui_invalid_email_toast);
						//							break;
						case ParseException.USERNAME_TAKEN:
							showToast(R.string.com_parse_ui_username_taken_toast);
							break;
						case ParseException.EMAIL_TAKEN:
							showToast(R.string.com_parse_ui_email_taken_toast);
							break;
						default:
							showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
						}
					}
				}
			});
		}
	}

	@Override
	protected String getLogTag() {
		return LOG_TAG;
	}

	private void signupSuccess() {
		onLoginSuccessListener.onLoginSuccess();
	}

}
