package com.parse.ui;

import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ui.ParseSignupFragment.ParseSignUpFragmentListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog.Calls;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class IncomingCallDetector extends PhoneStateListener {
	String incoming="";
	Context context;
	public ProgressDialog dialog;
	private String hash = "", id="", pin="";
	boolean validationStatus = false;
	private Object lock = new Object();
	Activity fragmentActivity;
	private ParseSignUpFragmentListener signUpFragmentListener;
	public IncomingCallDetector(Context c) {
		super();
		context = c;
	}
	private boolean isPhoneCalling = false;

	@Override
	public void onCallStateChanged(int state, String incomingNumber) {

		if (TelephonyManager.CALL_STATE_RINGING == state) {
			// phone ringing
			//Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
			incoming = incomingNumber;
			try{
				dialog.dismiss();
			}catch(Exception e){
				e.printStackTrace();
			}
			if(checkHash()){
				validateNumber(incoming.substring(incoming.length()-4));
				hangUpCall();
			}		

		}

		if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
			// active
			// Log.i(LOG_TAG, "OFFHOOK");

			isPhoneCalling = true;
		}

		if (TelephonyManager.CALL_STATE_IDLE == state) {
			// run when class initial and phone call ended, need detect flag
			// from CALL_STATE_OFFHOOK
			// Log.i(LOG_TAG, "IDLE number");

			if (isPhoneCalling) {

				Handler handler = new Handler();

				//Put in delay because call log is not updated immediately when state changed
				// The dialler takes a little bit of time to write to it 500ms seems to be enough
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// get start of cursor
						// Log.i("CallLogDetailsActivity", "Getting Log activity...");
						String[] projection = new String[]{Calls.NUMBER};
						Cursor cur = context.getContentResolver().query(Calls.CONTENT_URI, projection, null, null, Calls.DATE +" desc");
						cur.moveToFirst();
						String lastCallnumber = cur.getString(0);
					}
				},500);

				isPhoneCalling = false;
			}

		}
	}


	//synhronization code
	public void setIncomingNumber(String incomingNumber) { 


	} 

	public void loadingScreen(Map<String, String> data, String email, String password){
		dialog=new ProgressDialog(context);
		dialog.setMessage("Please wait while verifying...");
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setIndeterminate(true);
		dialog.show();
		hash = data.get("hash");
		id = data.get("id");
	}

	/**SHA1 hash function*/
	static String sha1(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}





	public boolean checkHash(){
		String incomingHash="";
		try {
			incomingHash = sha1(incoming.substring((incoming.length()-3)));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if(incomingHash.contentEquals(hash)){
			return true;
		}
		return false;
	}

	public void validateNumber(String pin){
		HashMap<String, String> validate = new HashMap<String, String>();
		validate.put("request", id);
		validate.put("pin", incoming.substring(incoming.length()-4));
		ParseCloud.callFunctionInBackground("PVValidate", validate, new FunctionCallback<Boolean>() {

			@Override
			public void done(Boolean status, ParseException e) {
				if(e == null){
					validationStatus = status;
				}else{
					e.printStackTrace();
				}
			}
		});
	}

	//	public void createAccount(){
	//		ParseUser user = new ParseUser();
	//
	//		// Set standard fields
	//		user.setUsername(email);
	//		user.setPassword(password);
	//		user.setEmail(email);
	//		user.put("phoneNumber", phoneNumber);
	//
	//		// Set additional custom fields only if the user filled it out
	//		if (firstName.length() != 0) {
	//			user.put("firstName", firstName);
	//		}
	//		if (lastName.length() != 0) {
	//			user.put("lastName", lastName);
	//		}
	//		fragmentActivity.loadingStart();
	//		user.signUpInBackground(new SignUpCallback() {
	//
	//			@Override
	//			public void done(ParseException e) {
	//				if (isActivityDestroyed()) {
	//					return;
	//				}
	//
	//				if (e == null) {
	//					loadingFinish();
	//					signupSuccess();
	//				} else {
	//					loadingFinish();
	//					if (e != null) {
	//						debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
	//								e.toString());
	//						switch (e.getCode()) {
	//						case ParseException.INVALID_EMAIL_ADDRESS:
	//							showToast(R.string.com_parse_ui_invalid_email_toast);
	//							break;
	//						case ParseException.USERNAME_TAKEN:
	//							showToast(R.string.com_parse_ui_username_taken_toast);
	//							break;
	//						case ParseException.EMAIL_TAKEN:
	//							showToast(R.string.com_parse_ui_email_taken_toast);
	//							break;
	//						default:
	//							showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
	//						}
	//					}
	//				}
	//			}
	//		});
	//	}

	public int disconnectCall()
	{
		Runtime runtime = Runtime.getRuntime();
		int nResp = 0;  
		try
		{    
			//Log.d(Keys.LOGTAG, "service call phone 5 \n");
			runtime.exec("service call phone 5 \n"); 
			System.out.println("HANG UP!!!");
		}catch(Exception exc)
		{
			//Log.e(Keys.LOGTAG, exc.getMessage());
			exc.printStackTrace();
		}
		return nResp;
	}


	public void hangUpCall(){
		try {

			String serviceManagerName = "android.os.ServiceManager";
			String serviceManagerNativeName = "android.os.ServiceManagerNative";
			String telephonyName = "com.android.internal.telephony.ITelephony";
			Class<?> telephonyClass;
			Class<?> telephonyStubClass;
			Class<?> serviceManagerClass;
			Class<?> serviceManagerNativeClass;
			Method telephonyEndCall;
			Object telephonyObject;
			Object serviceManagerObject;
			telephonyClass = Class.forName(telephonyName);
			telephonyStubClass = telephonyClass.getClasses()[0];
			serviceManagerClass = Class.forName(serviceManagerName);
			serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
			Method getService = // getDefaults[29];
					serviceManagerClass.getMethod("getService", String.class);
			Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
			Binder tmpBinder = new Binder();
			tmpBinder.attachInterface(null, "fake");
			serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
			IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
			Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
			telephonyObject = serviceMethod.invoke(null, retbinder);
			telephonyEndCall = telephonyClass.getMethod("endCall");
			telephonyEndCall.invoke(telephonyObject);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

}