
package com.parse.ui;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ui.ParseSignupFragment.ParseSignUpFragmentListener;

/**
 * Fragment for the user signup screen.
 */
public class EnterVerificationCodeFragment extends ParseLoginFragmentBase {

	public static final String FIRSTNAME = "com.parse.ui.EnterVerificationCodeFragment.FIRSTNAME";
	public static final String LASTNAME = "com.parse.ui.EnterVerificationCodeFragment.LASTNAME";
	public static final String PASSWORD = "com.parse.ui.EnterVerificationCodeFragment.PASSWORD";
	public static final String EMAIL = "com.parse.ui.EnterVerificationCodeFragment.EMAIL";
	public static final String BIRTHYEAR = "com.parse.ui.EnterVerificationCodeFragment.BIRTHYEAR";
	public static final String GENDER = "com.parse.ui.EnterVerificationCodeFragment.GENDER";
	public static final String ID = "com.parse.ui.EnterVerificationCodeFragment.ID";
	public static final String PHONENUMBER = "com.parse.ui.EnterVerificationCodeFragment.PHONENUMBER";
	//incoming call detector
	IncomingCallDetector phoneListener;
	public boolean takeLock = false;
	boolean validationStatus = false;
	private Object lock = new Object();
	private String codeField;
	private Button verifyPhoneNumberButton;
	private ParseOnLoginSuccessListener onLoginSuccessListener;

	private ParseLoginConfig config;
	private int minPhoneNumberLength;

	private static final String LOG_TAG = "EnterVerificationCode";
	private static final String USER_OBJECT_NAME_FIELD = "name";
	private static final int DEFAULT_MIN_PHONENUMBER_LENGTH = 8;
	private String firstName = "";
	private String lastName = "";
	private String password = "";
	private String email = "";
	private String birthyear = "";
	private String gender = "";
	private String phoneNumber = "";
	private String id = "";

	Typeface varella;
	Typeface montserrat;

	ProgressDialog pDialog;
	Context mContext;

	public static EnterVerificationCodeFragment newInstance(Bundle configOptions, 
			String firstName, String lastName,String password, String email, String birthyear, String gender, String id, String phoneNumber ) {
		EnterVerificationCodeFragment signupFragment = new EnterVerificationCodeFragment();
		Bundle args = new Bundle(configOptions);
		args.putString(EnterVerificationCodeFragment.FIRSTNAME, firstName);
		args.putString(EnterVerificationCodeFragment.LASTNAME, lastName);
		args.putString(EnterVerificationCodeFragment.PASSWORD, password);
		args.putString(EnterVerificationCodeFragment.EMAIL, email);
		args.putString(EnterVerificationCodeFragment.BIRTHYEAR, birthyear);
		args.putString(EnterVerificationCodeFragment.GENDER, gender);
		args.putString(EnterVerificationCodeFragment.ID, id);
		args.putString(EnterVerificationCodeFragment.PHONENUMBER, phoneNumber);
		signupFragment.setArguments(args);
		return signupFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup parent,
			Bundle savedInstanceState) {

		Bundle args = getArguments();
		config = ParseLoginConfig.fromBundle(args, getActivity());
		minPhoneNumberLength = DEFAULT_MIN_PHONENUMBER_LENGTH;

		View v = inflater.inflate(R.layout.enter_verification_code,
				parent, false);

		mContext = parent.getContext();

		varella = Typeface.createFromAsset(mContext.getAssets(), "VarelaRound.ttf");
		montserrat = Typeface.createFromAsset(mContext.getAssets(), "Montserrat-Regular.ttf");
		ImageView appLogo = (ImageView) v.findViewById(R.id.app_logo);


		firstName = (String) args.getString(FIRSTNAME);
		lastName = (String) args.getString(LASTNAME);
		password = (String) args.getString(PASSWORD);
		email = (String) args.getString(EMAIL).trim();
		birthyear = (String) args.getString(BIRTHYEAR);
		gender = (String) args.getString(GENDER);
		id = (String) args.getString(ID);
		phoneNumber = (String) args.getString(PHONENUMBER);

		TextView redoCode = (TextView) v.findViewById(R.id.redo_code_instructions);
		redoCode.setTypeface(varella);

		final EditText codeEntered = (EditText) v.findViewById(R.id.code_input);
		codeEntered.setTypeface(varella);
		TextView codeInstructions = (TextView) v.findViewById(R.id.code_instructions);
		codeInstructions.setTypeface(varella);

		Button confirmCodeButton = (Button) v.findViewById(R.id.confirm_code_button);
		confirmCodeButton.setTypeface(montserrat);
		confirmCodeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				codeField = codeEntered.getText().toString();
				//confirm the code
				validateNumber(codeField);
			}
		});

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof ParseOnLoginSuccessListener) {
			onLoginSuccessListener = (ParseOnLoginSuccessListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}


		if (activity instanceof ParseOnLoadingListener) {
			onLoadingListener = (ParseOnLoadingListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoadingListener");
		}
	}





	@Override
	protected String getLogTag() {
		return LOG_TAG;
	}

	private void signupSuccess() {
		onLoginSuccessListener.onLoginSuccess();
	}

	public void createAccountCloudFunction(){
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("email", email);
		params.put("username", email);
		params.put("password", password);
		params.put("phonenumber",  phoneNumber);
		params.put("firstname", firstName);
		params.put("lastname", lastName);
		params.put("gender", gender);
		params.put("age", Integer.parseInt(birthyear.trim()));
		ParseCloud.callFunctionInBackground("signup", params, new FunctionCallback<Boolean>() {

			@Override
			public void done(Boolean success, ParseException e) {
				if (isActivityDestroyed()) {
					return;
				}

				if(e == null){
					if(success){ 
						//successfully signed up user

						final ProgressDialog mDialog = new ProgressDialog(mContext);
						mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
						mDialog.setMessage("Creating account...");
						mDialog.show();

						ParseUser.logInInBackground(email, password, new LogInCallback() {
							@Override
							public void done(ParseUser user, ParseException e) {
								
								if (isActivityDestroyed()) {
									return;
								}

								if(e == null){
									HashMap<String, String> params = new HashMap<String, String>();
									ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
										@Override
										public void done(String object, ParseException e1) {
											if(mDialog.isShowing()){
												mDialog.cancel();
											}
											if(e1 == null){
												//logged in, so return

												loadingFinish();
												signupSuccess();

											}else{
												e1.printStackTrace();
												showToast("Error creating account, please try again.");
											}

										}
									});
								}else{
									e.printStackTrace();
									showToast("Error logging in.  Please try again.");
								}
							}
						});

						//new LogInAsyncTask(mContext).execute();
					}
				}else{
					e.printStackTrace();
					loadingFinish();
					debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
							e.toString());
					switch (e.getCode()) {
					//						case ParseException.INVALID_EMAIL_ADDRESS:
					//							showToast(R.string.com_parse_ui_invalid_email_toast);
					//							break;
					case ParseException.USERNAME_TAKEN:
						showToast(R.string.com_parse_ui_username_taken_toast);
						break;
					case ParseException.EMAIL_TAKEN:
						showToast(R.string.com_parse_ui_email_taken_toast);
						break;
					default:
						showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
					}
				}
			}
		});
	}


	public void createAccountNotInBack(){
		ParseUser user = new ParseUser();

		// Set standard fields
		user.setUsername(email);
		user.setPassword(password);
		user.setEmail(email);
		user.put("phoneNumber", "1" + phoneNumber); //add country code later

		// Set additional custom fields only if the user filled it out
		if (firstName.length() != 0) {
			user.put("firstName", firstName);
		}
		if (lastName.length() != 0) {
			user.put("lastName", lastName);
		}
		if (gender.length() != 0) {
			user.put("gender", gender);
		}
		if (birthyear.length() != 0) {
			user.put("age", Integer.parseInt(birthyear.trim()));
		}

		try{
			user.signUp();
			loadingFinish();
			signupSuccess();
			HashMap<String, String> params = new HashMap<String, String>();
			ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
				@Override
				public void done(String object, ParseException e1) {
					e1.printStackTrace();
				}
			});
		}catch(ParseException e){
			e.printStackTrace();
			loadingFinish();
			debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
					e.toString());
			switch (e.getCode()) {
			//						case ParseException.INVALID_EMAIL_ADDRESS:
			//							showToast(R.string.com_parse_ui_invalid_email_toast);
			//							break;
			case ParseException.USERNAME_TAKEN:
				showToast(R.string.com_parse_ui_username_taken_toast);
				break;
			case ParseException.EMAIL_TAKEN:
				showToast(R.string.com_parse_ui_email_taken_toast);
				break;
			default:
				showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
			}
		}

		if (isActivityDestroyed()) {
			return;
		}
	}


	public void createAccount(){
		ParseUser user = new ParseUser();

		// Set standard fields
		user.setUsername(email);
		user.setPassword(password);
		user.setEmail(email);
		user.put("phoneNumber", "1" + phoneNumber); //add country code later

		// Set additional custom fields only if the user filled it out
		if (firstName.length() != 0) {
			user.put("firstName", firstName);
		}
		if (lastName.length() != 0) {
			user.put("lastName", lastName);
		}
		if (gender.length() != 0) {
			user.put("gender", gender);
		}
		if (birthyear.length() != 0) {
			user.put("age", Integer.parseInt(birthyear.trim()));
		}
		//		loadingStart();
		user.signUpInBackground(new SignUpCallback() {

			@Override
			public void done(ParseException e) {
				if (isActivityDestroyed()) {
					return;
				}

				if (e == null) {
					loadingFinish();

					signupSuccess();
					HashMap<String, String> params = new HashMap<String, String>();
					ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
						@Override
						public void done(String object, ParseException e) {

						}
					});
				} else {
					loadingFinish();
					if (e != null) {
						debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
								e.toString());
						switch (e.getCode()) {
						//						case ParseException.INVALID_EMAIL_ADDRESS:
						//							showToast(R.string.com_parse_ui_invalid_email_toast);
						//							break;
						case ParseException.USERNAME_TAKEN:
							showToast(R.string.com_parse_ui_username_taken_toast);
							break;
						case ParseException.EMAIL_TAKEN:
							showToast(R.string.com_parse_ui_email_taken_toast);
							break;
						default:
							showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
						}
					}
				}
			}
		});
	}

	public void validateNumber(String pin){

		//dialog box
		loadingStart();

		HashMap<String, String> validate = new HashMap<String, String>();
		validate.put("request", id);
		validate.put("pin", pin);
		ParseCloud.callFunctionInBackground("PVValidate", validate, new FunctionCallback<Boolean>() {

			@Override
			public void done(Boolean status, ParseException e) {
				if(e == null){
					if(status){
						createAccountCloudFunction();
						//createAccountNotInBack();
						//createAccount();
					}
				}else{
					e.printStackTrace();
				}
			}
		});
	}

	//	public class LogInAsyncTask extends AsyncTask<Void, Void, Void>{
	//
	//		ProgressDialog mDialog;
	//		boolean timeOut = false;
	//		ParseException logInException = null;
	//
	//		public LogInAsyncTask(Context context){
	//			mDialog = new ProgressDialog(context);
	//			mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	//			mDialog.setMessage("Creating account...");
	//		}
	//
	//		@Override
	//		protected void onPreExecute() {
	//			mDialog.show();
	//		}
	//
	//		@Override
	//		protected Void doInBackground(Void... params) {
	//
	//			long seconds5  = 5000;
	//			final long startTime = System.currentTimeMillis();
	//
	//			try {
	//				ParseUser.logIn(email, password);
	//			} catch (ParseException e1) {
	//				logInException = e1;
	//			}
	//
	//			if(logInException == null){
	//				while(Math.abs(System.currentTimeMillis() - startTime) < seconds5){
	//
	//					if(ParseUser.getCurrentUser() != null){
	//						break;
	//					}
	//
	//				}
	//				timeOut = true;
	//			}
	//			return null;
	//		}
	//
	//		@Override
	//		protected void onPostExecute(Void param){
	//			if(mDialog.isShowing()){
	//				mDialog.cancel();
	//			}
	//
	//			if(logInException == null){
	//				if(timeOut == false){
	//					HashMap<String, String> params = new HashMap<String, String>();
	//					ParseCloud.callFunctionInBackground("tutorialMessage", params, new FunctionCallback<String>() {
	//						@Override
	//						public void done(String object, ParseException e) {
	//							if(e == null){
	//
	//							}else{
	//								e.printStackTrace();
	//							}
	//						}
	//					});
	//
	//					loadingFinish();
	//					signupSuccess();
	//				}else{
	//					showToast("Creating account timed out.  Please try again");
	//				}
	//			}else{
	//				logInException.printStackTrace();
	//				showToast("Error creating account, please try again.");
	//			}
	//
	//		}
	//
	//	}


}
