package com.parse.ui;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Fragment for the user signup screen.
 */
public class SignupEmailAndPassword extends ParseLoginFragmentBase {
	public interface SignupEmailAndPasswordListener {
		public void onNextClicked(String password, String email, String picswitchUsername);

		public void onLoginHelpClicked();

		public void onLoginSuccess();
	}

	public static final String USERNAME = "com.parse.ui.ParseSignupFragment.USERNAME";
	public static final String PASSWORD = "com.parse.ui.ParseSignupFragment.PASSWORD";
	public static final String PICSWITCH_USERNAME = "com.parse.ui.ParseSignupFragment.PICSWITCH_USERNAME";

	private EditText passwordField;
	private EditText confirmPasswordField;
	private EditText emailField;
	private EditText picswitchUsernameField;
	private Button nextButton;
	private ParseOnLoginSuccessListener onLoginSuccessListener;
	private SignupEmailAndPasswordListener signUpFragmentListener; 
	private ParseLoginConfig config;
	private int minPasswordLength;
	private ProgressDialog dialog;

	private static final String LOG_TAG = "ParseSignupFragment";
	private static final int DEFAULT_MIN_PASSWORD_LENGTH = 6;
	private static final String USER_OBJECT_NAME_FIELD = "name";

	Typeface varella;
	Typeface montserrat;

	public static SignupEmailAndPassword newInstance(Bundle configOptions, String username, String password) {
		SignupEmailAndPassword signupFragment = new SignupEmailAndPassword();
		Bundle args = new Bundle(configOptions);
		args.putString(SignupEmailAndPassword.USERNAME, username);
		args.putString(SignupEmailAndPassword.PASSWORD, password);
		signupFragment.setArguments(args);
		return signupFragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {

		Bundle args = getArguments();
		config = ParseLoginConfig.fromBundle(args, getActivity());

		minPasswordLength = DEFAULT_MIN_PASSWORD_LENGTH;
		if (config.getParseSignupMinPasswordLength() != null) {
			minPasswordLength = config.getParseSignupMinPasswordLength();
		}

		String username = (String) args.getString(USERNAME);
		String password = (String) args.getString(PASSWORD);

		View v = inflater.inflate(R.layout.sign_up_form_email_and_password_fragment,
				parent, false);

		varella = Typeface.createFromAsset(parent.getContext().getAssets(), "VarelaRound.ttf");
		montserrat = Typeface.createFromAsset(parent.getContext().getAssets(), "Montserrat-Regular.ttf");
		
		TextView emailDisclaimer = (TextView) v.findViewById(R.id.email_disclaimer);
		emailDisclaimer.setText(Html.fromHtml("Your email address will be used to log into PicSwitch.  We promise to <u>never</u> send you an email unless you win a prize."));
		emailDisclaimer.setTypeface(varella);

		ImageView appLogo = (ImageView) v.findViewById(R.id.app_logo);

		passwordField = (EditText) v.findViewById(R.id.signup_password_input);
		passwordField.setTypeface(varella);
		
		picswitchUsernameField = (EditText) v.findViewById(R.id.sign_up_picswitch_username_input);
		picswitchUsernameField.setTypeface(varella);

		confirmPasswordField = (EditText) v
				.findViewById(R.id.signup_confirm_password_input);
		confirmPasswordField.setTypeface(varella);

		emailField = (EditText) v.findViewById(R.id.sign_up_email_input);
		emailField.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		emailField.setTypeface(varella);

		nextButton = (Button) v.findViewById(R.id.next_button_to_name_input);
		nextButton.setTypeface(montserrat);

		emailField.setText(username);
		passwordField.setText(password);

		if (appLogo != null && config.getAppLogo() != null) {
			appLogo.setImageResource(config.getAppLogo());
		}

		if (config.isParseLoginEmailAsUsername()) {
			if (emailField != null) {
				emailField.setVisibility(View.GONE);
			}
		}


		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog=new ProgressDialog(getActivity());
				dialog.setMessage("Please wait while checking availability...");
				dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				dialog.setIndeterminate(true);

				if(emailField.getText().toString()!=null && emailField.getText().toString().length() ==0){
					showToast(R.string.com_parse_ui_no_email_toast);
				}else if(picswitchUsernameField.getText().toString().contains(" ")){
					showToast("Username cannot contain spaces.");
				}else{
					dialog.show();
					checkAvailability(emailField.getText().toString(), picswitchUsernameField.getText().toString());
				}


			}
		});

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof ParseOnLoginSuccessListener) {
			onLoginSuccessListener = (ParseOnLoginSuccessListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}
		if (activity instanceof ParseOnLoginSuccessListener) {
			signUpFragmentListener = (SignupEmailAndPasswordListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoginSuccessListener");
		}

		if (activity instanceof ParseOnLoadingListener) {
			onLoadingListener = (ParseOnLoadingListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implemement ParseOnLoadingListener");
		}
	}



	public void checkAvailability(final String emailParam, String picswitchUsername){

		//check validity of email here.		
		boolean validEmailForm = false;
		
		final String email = emailParam.trim();
		System.out.println("email after trim : " + email);
		
		int countAts = email.length() - email.replace("@", "").length();
		int countSpace = email.length() - email.replace(" ", "").length();
		
		String secondHalf = "";
		int countPeriods = 0;
		
		if(countAts == 1){
			secondHalf = email.substring(email.indexOf("@") + 1, email.length());
			countPeriods = secondHalf.length() - secondHalf.replace(".", "").length();
		}
		
		if( !(email.substring(0, 1).equals(".")) && (countAts == 1) && (countSpace == 0) && (countPeriods > 0)){
			validEmailForm = true;
		}else{
			if(email.substring(0, 1).equals(".")){
				showToast("Invalid email: email cannot begin with a period.");
			}else if(countAts != 1){
				showToast("Invalid email: email must have one @ symbol.");
			}else if(countSpace != 0){
				showToast("Invalid email: email cannot contain spaces.");
			}else if(countPeriods == 0){
				showToast("Invalid email: email must have at least one period after @.");
			}
		}
		
		if(validEmailForm){
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("picswitchUsername", picswitchUsername);
			query.findInBackground(new FindCallback<ParseUser>() {

				@Override
				public void done(List<ParseUser> objects, ParseException e) {
					if(e==null){
						if(objects.size()!=0){//picswitch username already taken
							showToast("Username already taken, please choose another username");
						}else{
							ParseQuery<ParseUser> queryUsername = ParseUser.getQuery();
							queryUsername.whereEqualTo("username", email);
							queryUsername.findInBackground(new FindCallback<ParseUser>(){
								@Override
								public void done(List<ParseUser> objects, ParseException e) {
									if(e==null){
										if(objects.size()==0){//username not taken
											createAccount();
										}else{
											showToast("Email already taken, please use another email address");
										}
									}
								}
							});
						}
					}
				}
			});
		}
		dialog.dismiss();
	}
	
	
	public void createAccount() {
		String email = emailField.getText().toString();
		String password = passwordField.getText().toString();
		String passwordAgain = confirmPasswordField.getText().toString();
		String picswitchUsername = picswitchUsernameField.getText().toString();
		email = emailField.getText().toString();

		if (email != null && email.length() == 0) {
			showToast(R.string.com_parse_ui_no_email_toast);
		} 
		else if (password.length() == 0) {
			showToast(R.string.com_parse_ui_no_password_toast);
		} else if (password.length() < minPasswordLength) {
			showToast(getResources().getQuantityString(
					R.plurals.com_parse_ui_password_too_short_toast,
					minPasswordLength, minPasswordLength));
		} else if (passwordAgain.length() == 0) {
			showToast(R.string.com_parse_ui_reenter_password_toast);
		} else if (!password.equals(passwordAgain)) {
			showToast(R.string.com_parse_ui_mismatch_confirm_password_toast);
			confirmPasswordField.selectAll();
			confirmPasswordField.requestFocus();
		}  


		else{
			signUpFragmentListener.onNextClicked(email, password, picswitchUsername);
		}
		//		signUpFragmentListener.onVerifyPhoneNumberClicked(firstName, lastName, password, email);
		//		else {
		//			ParseUser user = new ParseUser();
		//
		//			// Set standard fields
		//			user.setUsername(email);
		//			user.setPassword(password);
		//			user.setEmail(email);
		//
		//			// Set additional custom fields only if the user filled it out
		//			if (firstName.length() != 0) {
		//				user.put("firstName", firstName);
		//			}
		//			if (lastName.length() != 0) {
		//				user.put("lastName", lastName);
		//			}
		//			loadingStart();
		//			user.signUpInBackground(new SignUpCallback() {
		//
		//				@Override
		//				public void done(ParseException e) {
		//					if (isActivityDestroyed()) {
		//						return;
		//					}
		//
		//					if (e == null) {
		//						loadingFinish();
		//						signupSuccess();
		//					} else {
		//						loadingFinish();
		//						if (e != null) {
		//							debugLog(getString(R.string.com_parse_ui_login_warning_parse_signup_failed) +
		//									e.toString());
		//							switch (e.getCode()) {
		//							case ParseException.INVALID_EMAIL_ADDRESS:
		//								showToast(R.string.com_parse_ui_invalid_email_toast);
		//								break;
		//							case ParseException.USERNAME_TAKEN:
		//								showToast(R.string.com_parse_ui_username_taken_toast);
		//								break;
		//							case ParseException.EMAIL_TAKEN:
		//								showToast(R.string.com_parse_ui_email_taken_toast);
		//								break;
		//							default:
		//								showToast(R.string.com_parse_ui_signup_failed_unknown_toast);
		//							}
		//						}
		//					}
		//				}
		//			});
		//		}
	}

	@Override
	protected String getLogTag() {
		return LOG_TAG;
	}

}
